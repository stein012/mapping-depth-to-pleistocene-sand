Example code with *Mapping depth to Pleistocene sand with Bayesian generalised linear geostatistical models*   
========================================

Authors: [Luc Steinbuch](https://orcid.org/0000-0001-6484-0920)^1,2,3^,
[Dick J. Brus](https://orcid.org/0000-0003-2194-4783)^4^,
[Gerard B. M. Heuvelink](https://orcid.org/0000-0003-0959-9358)^1,2^

^1^ Soil Geography and Landscape group, Wageningen University and Research, Netherlands;  
^2^ ISRIC -- World Soil Information, Wageningen, Netherlands;  
^3^ Wageningen Environmental Research, Wageningen University and Research, Netherlands;  
^4^ Biometris, Wageningen University and Research, Netherlands. 

Corresponding author: Luc.Steinbuch\@wur.nl 

## Keywords

Bayesian statistics; geostatistics; spatial statistics; generalised linear spatial models; ; hierarchical spatial models; binomial obervations



## General

Spatial soil applications frequently involve binomial variables. This code in the R language shows the application of  a Bayesian generalised linear model (BGLM; which under certain preconditions equals a generalised linear model) as well as its extension, a Bayesian generalised linear geostatistical model (BGLGM), used on a simulated dataset. The output includes posterior parameters and spatial prediction maps.

This code shows the principles as presented in the paper *Mapping depth to Pleistocene sand with Bayesian generalised linear geostatistical models* (Steinbuch, Brus, Heuvelink; 2021, European Journal of Soil Science, DOI: [10.1111/ejss.13140](https://doi.org/10.1111/ejss.13140) ), using simulated data. The original observed data (calibration- and validation observations, covariates) are not included, but available upon request; so is the code to reproduce specific plots and the table.

A maintained version of this dataset can be found at https://git.wur.nl/stein012/mapping-depth-to-pleistocene-sand .

Note that all self created R-functions are located in a separate file with the same name as the function.


## Included files

* **main.R** : Simulates data, then shows the principles as mentioned in the paper with successively BGLM calibration, prediction and analysis, and BGLGM calibration and analysis. All other *.R files are called from this script.
* **generate_data.R**: function, generates calibration data and prediction mask
* **compress_binomial_observations.R**: function, reduces the number of observation locations by joining those
* **calculate_reference_prior.R**: function, calculates the reference prior for range parameter phi
* **run_mcmc.R**: function, extended wrapper around the core calculation function binom.krige.bayes from package geoRglm. Contains a PID controller to tune the proposal distribution parameters
* **plot_traceplot_and_density.R**: function, to support plotting the MCMC outcomes
* **All_Results.RData**: because the MCMC run can take half an hour to run, the results are already provided for final analysis


## License

The presented example code is available under the European Union Public License, version 1.2 (EUPL-1.2). Referring to the frozen repository ([DOI: 10.5281/zenodo.5005828](https://doi.org/10.5281/zenodo.5005828)), and/or this repository ([https://git.wur.nl/stein012/mapping-depth-to-pleistocene-sand](https://git.wur.nl/stein012/mapping-depth-to-pleistocene-sand)) and/or to the related paper ([DOI: 10.1111/ejss.13140](https://doi.org/10.1111/ejss.13140)) is highly appreciated.


## Version information

Running `sessionInfo()`:

```
R version 4.0.3 (2020-10-10)
Platform: x86_64-w64-mingw32/x64 (64-bit)
Running under: Windows 10 x64 (build 19042)

Matrix products: default

locale:
[1] LC_COLLATE=Dutch_Netherlands.1252  LC_CTYPE=Dutch_Netherlands.1252    LC_MONETARY=Dutch_Netherlands.1252
[4] LC_NUMERIC=C                       LC_TIME=Dutch_Netherlands.1252    

 attached base packages:
[1] grid      stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
[1] viridisLite_0.3.0          tictoc_1.0                 PerformanceAnalytics_2.0.4 xts_0.12.1                
[5] zoo_1.8-9                  geoRglm_0.9-16             geoR_1.8-1                 sp_1.4-5                  

loaded via a namespace (and not attached):
[1] quadprog_1.5-8          lattice_0.20-41         MASS_7.3-53             RandomFields_3.3.8     
[5] tools_4.0.3             splancs_2.01-40         compiler_4.0.3          tcltk_4.0.3            
[9] RandomFieldsUtils_0.5.3
```