# Example code with *Mapping depth to Pleistocene sand with Bayesian generalised linear geostatistical models* 
# by Steinbuch, Brus, Heuvelink, 2021, European Journal of Soil Science;  doi.org/10.1111/ejss.13140

# June 2021, Wageningen University and Research, ISRIC -- World Soil Information


#### Load packages etc.####

library("geoR")

## *****************
# During final testing we found out that package geoRglm is currently not maintained and can as such not 
# be installed as a normal package. Remedy:
# 0. on Windows, you  need to install RTools  (https://cran.rstudio.com/bin/windows/Rtools/)
# 1. Install and activate package devtools
# 2. run:   install_version("geoRglm", version = "0.9.16", repos = "http://cran.us.r-project.org")
#
# Quoting  https://genetics.ghpc.au.dk/ofch/geoRglm.html, accessed May 7th 2021:
# "Package geoRglm is no longer being actively developed, but critical bugs will be fixed" 
# (This page was last updated January 12th, 2021). 
#
## *****************

library("geoRglm")   
library("grid")
library("PerformanceAnalytics")
library("sp")
library("tictoc")  
library("viridis")


## Define color function for probability maps
colfunc<-colorRampPalette(c("red", "lightsalmon",  "gray90", "deepskyblue", "blue"),
                          interpolate = c("spline")
                          ) 

## Define colors for compressed observations, and related label
vcol_compressed_observations <- c("red", "orange", "black", "lightblue", "blue")
vs_compressed_observations   <- c("All fail", "3 fail, 1 success", "2 fail, 2 success", "1 fail, 3 success", "All success")

#### >>>> Generate data ####

source("generate_data.R")

set.seed(56826)
li_generated <- generate_data(
  bo_irreg  = TRUE,
  n_sigmasq = 3,
  n_phi     = 20,
  n_beta_0  = -10,
  n_beta_1  = 0.1)

## This function generates simulated "observations" (=calibration data) 
#  and a prediction field, both with one covariate directly dependent 
#  on coordinates

geod_cali <- li_generated$geod_cali

df_simulation_field <- li_generated$df_simulation_field
df_prediction_field <- li_generated$df_prediction_field 


#### Plot calibration data ####

## For plotting, the sp-package is easier
#  thus we create a "spatial points data frame" object


spdf_simulation_field <- df_simulation_field
coordinates(spdf_simulation_field) <- ~ x + y

print(
  spplot(spdf_simulation_field,
         zcol = "data",
         col.regions = colfunc(2),
         scales = list(draw = TRUE),
         auto.key = FALSE,
         main = "Simulated calibration data"
         
  )
)


#### Plot covariate for prediction  ####


## In this case, a spatial pixels data frame is most convenient

spdf_prediction_field <- df_prediction_field
coordinates(spdf_prediction_field) <- ~ x + y

spixdf_prediction_field <- SpatialPixelsDataFrame(points = df_prediction_field[,c(1,2)],
                                                  data   = as.data.frame(df_prediction_field[,3])
                                                  )


# range(df_prediction_field[,3])
print(
  spplot(spixdf_prediction_field,
         col.regions = viridis(100),
         at = seq(from = 0, 
                  to = 200, 
                  length.out = 100),
         scales = list(draw = TRUE),
         key.space="right",
         cuts = 20,
         main = "Covariate"         
         
  )
)



#### >>>> BGLM analysis and prediction ####
## With the given prior, BGLM equals GLM,
## thus we can use the base glm function

## Calibrate BGLM

bglm_model <- 
  glm(formula = data ~ covariate,
      data    = df_simulation_field,
      family  = binomial(link = "logit")
  )

summary(bglm_model)


## Plot BGLM posterior beta


for(n_beta in 1:2)
{
  n_mean <- coefficients(bglm_model)[n_beta]
  n_sd   <- sqrt(diag(vcov(bglm_model)))[n_beta]
  
  n_plot_min <- n_mean - 5*n_sd
  n_plot_max <- n_mean + 5*n_sd
  
  curve(dnorm(x,
              mean = n_mean,
              sd = n_sd
              ),
    from = n_plot_min,
    to = n_plot_max,
    n = 200,
    col = "blue",
    xlab = bquote(beta[.(n_beta-1)]),
    ylab = "Density",
    main = bquote(BGLM~posterior~beta[.(n_beta-1)])
  )
  
  grid()
  
}

#### Predict with BGLM ####


## Calculate prediction 
vn_prediction_BGLM <- 
  predict(object = bglm_model,
          newdata = df_prediction_field,
          type = "response")


spixdf_prediction_field$prediction_bglm <- vn_prediction_BGLM 


## For clarification, we want to add some extra points on the map;
#  therefore we have to create list objects

li_points_cali = list('sp.points', 
                      spdf_simulation_field, 
                      cex = 0.4, 
                      pch = 1, 
                      col = c("red","blue")[spdf_simulation_field$data + 1]
)

li_points_cali_bg = list('sp.points', 
                         spdf_simulation_field, 
                         cex = 0.7, 
                         pch = 19, 
                         col = "white"
)




print(
  spplot(spixdf_prediction_field,
         zcol = "prediction_bglm",
         col.regions = colfunc(100),
         at = seq(from = 0, to = 1, length.out = 100),
         scales = list(draw = TRUE),
         #auto.key=TRUE
         #colorkey=TRUE
         key.space="right",
         main = "Prediction BGLM; observations",
         sp.layout=list(
           li_points_cali_bg, 
           li_points_cali
         )
         
  )
)


## Separate legend for observations

plot.new()

legend(x = "topleft",
       legend = c("Succes", "Fail"),
       col = c("blue", "red"),
       pch = 1
       )




#### >>>> BGLGM analysis and prediction ####


#### Condense observations ####


source("compress_binomial_observations.R")

# In this case we compress 2 times, to reduce the number of observation locations
# from 1156 to 289; this enormously reduces computational costs

geod_cali_compr <- compress_binomial_observations(geod_cali, 
                                                  vn_points_to_be_combined = c(1200,600),
                                                  bo_verbose = TRUE  # Print progress 
)



## Again: for plotting, create a spdf object

df_cali_compr <- as.data.frame(geod_cali_compr)

class(df_cali_compr) <- "data.frame"

spdf_cali_compr <- df_cali_compr
coordinates(spdf_cali_compr) <- ~ x + y


print(
  spplot(spdf_cali_compr,
         zcol = "data",
         col.regions = vcol_compressed_observations,
         scales = list(draw = TRUE),
         auto.key=FALSE,
         main = "Simulated calibration data, compressed / aggregated"
         
  )
)

## Separate legend

plot.new()

legend(x = "topleft",
       legend = vs_compressed_observations,
       col = vcol_compressed_observations,
       pch = 16
)



## Calculate reference prior for phi

source("calculate_reference_prior.R")


vn_prior_phi_values <- seq(from = 0.25,
                           to   = 25,
                           length.out = 25)
# Note that for later use, this vector must be equally spaced, and for
# calculation time not too large

vn_prior_phi_densities <- calculate_reference_prior(geod_cali_compr, 
                                                    vn_phi = vn_prior_phi_values,
                                                    bo_verbose = TRUE  # Print progress
)


plot(x = vn_prior_phi_values ,
     y = vn_prior_phi_densities,
     type = "l",
     xlab = expression(phi),
     ylab = "Density",
     main = expression(phi~reference~prior)
)
grid()

source("run_mcmc.R")

#### Define MCMC settings ####
li_general_settings <- list()

li_general_settings$li_mcmc_settings$bo_predict           <- TRUE 
## Note: "Predict" here for the purpose of statistical validation only
li_general_settings$li_mcmc_settings$n_warmup_iterations  <- 5e5
li_general_settings$li_mcmc_settings$n_final_iterations   <- 5e5
li_general_settings$li_mcmc_settings$n_final_thinned_iterations  <- 50

li_general_settings$li_mcmc_settings$mcmc_prior <- 
  prior.glm.control(
    sigmasq.prior  = "reciprocal",
    beta.prior     = "flat",
    phi.prior    = vn_prior_phi_densities,
    phi.discrete = vn_prior_phi_values,
    tausq.rel    = 0
  )



li_mcmc_settings_and_data <- list(li_mcmc_settings = li_general_settings$li_mcmc_settings, 
                                  geod_cali = geod_cali_compr,
                                  geod_pred_mask = li_generated$geod_prediction_field)  


li_mcmc_settings_and_data$li_mcmc_settings$mcmc_model <- 
  model.glm.control(
    cov.model = "exp" ,
    trend.l   = trend.spatial(~covariate, 
                              li_generated$geod_prediction_field), # prediction locations
    trend.d   = trend.spatial(~covariate, 
                              geod_cali_compr) # calibration
  )

li_mcmc_settings_and_data$li_mcmc_settings$n_set_seed <- 735


## The following command will take about half an hour to run -- and might sometimes seem frozen for several minutes. 
#  If you are rather interested in the final analysis, you can skip this line and load "All_Results.RData"

li_BGLGM_results <- run_mcmc(li_mcmc_settings_and_data)

save.image("All_Results.RData")


n_final_iterations  <- li_general_settings$li_mcmc_settings$n_final_iterations 
n_final_thinned_iterations <- li_general_settings$li_mcmc_settings$n_final_thinned_iterations



#### BGLGM traceplots and densities ####

source("plot_traceplot_and_density.R")


plot_traceplot_and_density(vn_posterior = li_BGLGM_results$glmkb_final$posterior$phi$sample,
               nm_variable = quote(phi),
               n_final_iterations = n_final_iterations,
               n_final_thinned_iterations = n_final_thinned_iterations
)

plot_traceplot_and_density(vn_posterior = li_BGLGM_results$glmkb_final$posterior$sigmasq$sample,
                nm_variable = quote(sigma^2),
                n_final_iterations = n_final_iterations,
                n_final_thinned_iterations = n_final_thinned_iterations
)

plot_traceplot_and_density(vn_posterior = li_BGLGM_results$glmkb_final$posterior$beta$sample[1,],
                nm_variable = quote(beta[0]),
                n_final_iterations = n_final_iterations,
                n_final_thinned_iterations = n_final_thinned_iterations
)

plot_traceplot_and_density(vn_posterior = li_BGLGM_results$glmkb_final$posterior$beta$sample[2,],
                nm_variable = quote(beta[1]),
                n_final_iterations = n_final_iterations,
                n_final_thinned_iterations = n_final_thinned_iterations
)


plot_traceplot_and_density(vn_posterior = li_BGLGM_results$glmkb_final$posterior$simulations[35,],
                           nm_variable = quote(Signal~location~35),
                           n_final_iterations = n_final_iterations,
                           n_final_thinned_iterations = n_final_thinned_iterations
)





#### BGLGM Correlation in posterior parameters####

df_explore_correlation <- as.data.frame(
  cbind(li_BGLGM_results$glmkb_final$posterior$phi$sample,
        li_BGLGM_results$glmkb_final$posterior$sigmasq$sample,
        t(li_BGLGM_results$glmkb_final$posterior$beta$sample)
  )
)
names(df_explore_correlation) <- c("phi", "sigmasq", "beta_0", "beta_1")


chart.Correlation(df_explore_correlation,
                          histogram=TRUE)


#### BGLGM prediction ####

## Prediction is the median of the thinned MCMC values
spixdf_prediction_field$prediction_bglgm <-  apply(X = li_BGLGM_results$glmkb_final$predictive$simulations,
                                                   MARGIN = 1, #columnwise
                                                   FUN = median)
  

## We want to show the -- compressed -- observation locations
li_points_cali_compr = list('sp.points', 
                      spdf_cali_compr, 
                      cex = 0.8, 
                      pch = 1, 
                      col = vcol_compressed_observations[spdf_cali_compr$data + 1]
                      )

li_points_cali_bg_compr = list('sp.points', 
                               spdf_cali_compr, 
                               cex = 1.4, 
                               pch = 19, 
                               col = "white"
                            )


## We also want to indicate the location used for the signal traceplot
vn_indicate_location_pch <- rep(NA, nrow(spdf_cali_compr))
vn_indicate_location_pch[c(35)] <- 4 # We indicate the location(s) with a big 

li_points_cali_trace = list('sp.points', 
                            spdf_cali_compr, 
                            cex = 8,
                            pch = vn_indicate_location_pch, 
                            col = "purple" 
                          )


  
print(
  spplot(spixdf_prediction_field,
         zcol = "prediction_bglgm",
         col.regions = colfunc(100),
         at = seq(from = 0, to = 1, length.out = 100),
         scales = list(draw = TRUE),
         key.space="right",
         main = "Prediction BGLGM, with compressed observations",
         sp.layout=list(
           li_points_cali_bg_compr, 
           li_points_cali_compr,
           li_points_cali_trace
         )
         
  )
)



## Separate legend

plot.new()

legend(x = "topleft",
       legend = c(vs_compressed_observations, "Used for traceplot"),
       col = c(vcol_compressed_observations, "purple"),
       pch = c(1,1,1,1,1,4)
)
