run_mcmc <- function(li_mcmc_settings_and_data)
{  
  
  ## Contains the tuning algorithm, based on a PID feedback controller
  
  #### >>>> General prepaparation ####
  
  
  geod_cali        <- li_mcmc_settings_and_data$geod_cali
  geod_pred_mask   <- li_mcmc_settings_and_data$geod_pred_mask
  li_mcmc_settings <- li_mcmc_settings_and_data$li_mcmc_settings
  n_final_iterations         <- li_mcmc_settings$n_final_iterations
  n_final_thinned_iterations <- li_mcmc_settings$n_final_thinned_iterations
  n_warmup_iterations        <- li_mcmc_settings$n_warmup_iterations
  str_export_tuning_iterations <- li_mcmc_settings$str_export_tuning_iterations # "all", "every1000", "onlylast"
  bo_predict                 <- li_mcmc_settings$bo_predict
  mcmc_prior  <- li_mcmc_settings$mcmc_prior
  mcmc_model  <- li_mcmc_settings$mcmc_model
  set.seed(seed = li_mcmc_settings$n_set_seed )
  
  if( is.na(n_final_iterations) )
    n_final_iterations <- 1e6
  
  if( is.na(n_final_thinned_iterations) )
    n_final_thinned_iterations <- 1e3
  
  if(bo_predict)
  {
    str_or_geod_pred_loc <- geod_pred_mask$coords
  } else
  {
    str_or_geod_pred_loc <- "no"
  }  
  
  # If run in a parallel environment, packages might have to be loaded again
  require(geoR)
  require(geoRglm)
  require(tictoc)    
  
  
  tic("total")
  
  logit <- function(p)
  {
    log(
      p/(1-p) 
    )
  }
  
  logit_lim <- function(p, lim = 70)
  {
    y <- log(
      p/(1-p) 
    )
    y <- ifelse(y > lim,   lim, y)
    y <- ifelse(y < -lim, -lim, y) 
    return(y)
  }
  
  ## Create design matrix, to be able to 'feed' S,  Yd based on 
  mn_X <- trend.spatial(mcmc_model$trend.d, geod_cali)
  
  n_k <- ncol(mn_X)  # number of covariates, including mean
  
  
  
  
  
  #### Define length of each test run during  tuning phase ####
  if(FALSE) ## Test settings
  {
    n_iterations_every_tuning_step <- 2000
    n_max_tuning_iterations <- 2
  }  else
  {
    n_iterations_every_tuning_step <- 10000
    n_max_tuning_iterations <- 30
    
  }
  
  
  #### Define acceptable range and setpoints (=desired value) for acceptance ranges during the tuning phase ####  
  vn_acc_rate_range_S = c(0.4, 0.65)  #Langevin-Hastings; optimal 0.57 ; Christensen 2006
  n_target_acceptance_rate_S <- mean(vn_acc_rate_range_S)  # in control terms: setpoint
  
  vn_acc_rate_range_phi = c(0.35, 0.6) # Metropolis-within-Gibbs; optimal 0.44 ; Rosenthal 2010/2011
  n_target_acceptance_rate_phi <- mean(vn_acc_rate_range_phi) 
  
  
  #### Estimate a fixed beta for internal use ####
  
  
  ## In case geod_cali$units.m is not defined yet, create it
  if( is.null(geod_cali$units.m) )
  {
    geod_cali$units.m <- rep(1, 
                             length.out = length(geod_cali$data) 
    )
  }
  
  
  vn_successes <- geod_cali$data
  vn_failures  <- geod_cali$units.m - vn_successes
  
  df_cali <- data.frame(successes = vn_successes,
                        failures = vn_failures
  )
  
  
  
  if (n_k == 1)
  {
    glm_fit <- glm(cbind(successes, failures) ~ 1,
                   family=binomial(),
                   data = df_cali
    )
  } else
  {
    glm_fit <- glm(cbind(successes, failures) ~ mn_X[,-1],
                   family=binomial(),
                   data = df_cali
    )  
  }
  vn_beta <- coef(glm_fit)
  
  remove(df_cali)
  
  
  #### >>>> PID find scale factor  #####
  
  #### Initiate df to trace/record the tuning phase ####
  m_trace_PID <- matrix(NA, nrow=0, ncol=8)
  
  df_trace_PID <- as.data.frame(
    matrix(NA, nrow=0, ncol=15)
  )
  
  names(df_trace_PID) <- c("tuning_iteration",
                           "used_scale_S",
                           "acc_rate_S",
                           "target_acceptance_rate_S",
                           "error_S",
                           "error_S_cum",
                           "error_S_dev",
                           "new_scale_S",
                           "used_scale_phi",
                           "acc_rate_phi",
                           "target_acceptance_rate_phi",                           
                           "error_phi",
                           "error_phi_cum",
                           "error_phi_dev",
                           "new_scale_phi"                           
  )
  
  # Arbitrary starting values
  n_S_scale   <- 1  
  n_phi_scale <- 1  
  
  
  ## Settings for tuning phase
  
  ## Set the controls for the MCMC chain outputs
  mcmc_output <-  output.glm.control(messages = TRUE,
                                     sim.posterior = TRUE,
                                     keep.mcmc.sim = TRUE,
                                     sim.predict = FALSE,
                                     quantile    = FALSE,
                                     inference   = FALSE) 
  
  
  
  str_export_tuning_iterations <- "every1000" # options: "all", "every1000", "onlylast"
  
  if(str_export_tuning_iterations == "all")
  {
    n_thin_warming_up <- 1
  } else if (str_export_tuning_iterations == "every1000")
  {
    n_thin_warming_up <- 1000
  } else # str_export_tuning_iterations == "onlylast"
  {
    n_thin_warming_up <- n_iterations_every_tuning_step  # only the last value
  }
  
  li_warming_up_results <- list()
  i <- 1
  ######## Tuning phase / PID loop START ########
  repeat 
  {
    
    cat("\n Tuning phase try nr: ", i, "\n")
    if(i==1) # Set starting values for phi, S (y in paper) and scaling parameters
    { 
      
      ## Decide starting value for phi
      if(mcmc_prior$phi.prior=="uniform" | length(mcmc_prior$phi.discrete)>1 ) 
        # uniform or vector of probabilities
      {
        n_phi_start <- sample(x = mcmc_prior$phi.discrete,
                              size = 1)
      } else if (mcmc_prior$phi.prior=="fixed")
      {
        n_phi_start <- mcmc_prior$phi
      } else
      {
        stop("No default start value phi defined for this prior for phi")
      }
      
      ## Set the controls for the MCMC chain
      mcmc_cntrl <- mcmc.control(S.scale = n_S_scale, 
                                 burn.in = 0, 
                                 thin    = n_thin_warming_up, 
                                 phi.scale = n_phi_scale, 
                                 S.start = "random", # startting values for S (y in paper), options: "default", #rep(x = -10, length.out = 1126),
                                 n.iter  = n_iterations_every_tuning_step,
                                 phi.start = n_phi_start
      )
    } else # reuse previous values for y (S) and phi, and apply the calculated new scales 
    { 
      # Determine the sequence number of the last recorded chain iteration
      n_last_thinned_step <- ncol(glmkb_tst$posterior$simulations)
      
      ## Calculate back the signal from the last provided probabilities
      vn_S <- logit_lim( glmkb_tst$posterior$simulations[,n_last_thinned_step] )
      vn_S <- vn_S - mn_X %*% vn_beta # remove the mean
      
      ## Get the last used phi
      n_phi <- glmkb_tst$posterior$phi$sample[n_last_thinned_step]
      
      ## Set the controls for the MCMC chain
      mcmc_cntrl <- mcmc.control(S.scale =   n_S_scale, 
                                 burn.in =   0, 
                                 thin    =   n_thin_warming_up, 
                                 phi.scale = n_phi_scale, 
                                 S.start  =  vn_S,
                                 n.iter   =  n_iterations_every_tuning_step,
                                 phi.start = n_phi
      )
      
    }    
    
    
    #### Run try MCMC as part of tuning phase ####
    glmkb_tst <- binom.krige.bayes(geod_cali,
                                   model  = mcmc_model,
                                   prior  = mcmc_prior,
                                   mcmc.input = mcmc_cntrl,
                                   output = mcmc_output)
    
    ## Store complete result for eventual later evaluation
    li_warming_up_results[[i]] <- glmkb_tst
    
    ## Calculate the mean acceptance rates of this try
    n_last_acc_rate_S <- mean(glmkb_tst$posterior$acc.rate[,2])
    if(mcmc_prior$phi.prior=="fixed")
      n_last_acc_rate_phi <- n_target_acceptance_rate_phi
    else
      n_last_acc_rate_phi <- mean(glmkb_tst$posterior$acc.rate[,3])
    
    
    ## Update dataframe with the new information; 
    # calculate depending measures such as proportional error, integrated error and differential error 
    # (P-I-D, for the PID controller: browseURL("https://en.wikipedia.org/wiki/PID_controller") )
    df_trace_PID[i, "tuning_iteration"] <- i
    df_trace_PID[i, "used_scale_S"]     <- n_S_scale
    df_trace_PID[i, "acc_rate_S"]       <- n_last_acc_rate_S
    df_trace_PID[i, "target_acceptance_rate_S"]       <- n_target_acceptance_rate_S
    
    n_error_S <- n_target_acceptance_rate_S - n_last_acc_rate_S
    df_trace_PID[i, "error_S"]          <- n_error_S    
    
    n_error_S_cum                       <- sum(df_trace_PID[1:i, "error_S"])
    df_trace_PID[i, "error_S_cum"]      <- n_error_S_cum
    
    
    df_trace_PID[i, "used_scale_phi"]     <- n_phi_scale
    df_trace_PID[i, "acc_rate_phi"]       <- n_last_acc_rate_phi
    df_trace_PID[i, "target_acceptance_rate_phi"]       <- n_target_acceptance_rate_phi
    
    n_error_phi <- n_target_acceptance_rate_phi - n_last_acc_rate_phi
    df_trace_PID[i, "error_phi"]          <- n_error_phi    
    
    n_error_phi_cum                       <- sum(df_trace_PID[1:i, "error_phi"])
    df_trace_PID[i, "error_phi_cum"]      <- n_error_phi_cum
    
    if(i>1)
    {
      n_error_phi_dev <- diff(df_trace_PID[c(i-1, i), "error_phi"])
    } else
    {
      n_error_phi_dev <- 0
    }
    
    df_trace_PID[i, "error_phi_dev"] <- n_error_phi_dev
    
    
    if(i>1)
    {
      n_error_S_dev <- diff(df_trace_PID[c(i-1, i), "error_S"])
    } else
    {
      n_error_S_dev <- 0
    }
    
    df_trace_PID[i, "error_S_dev"] <- n_error_S_dev    
    
    #### Escape repeat loop if acceptable acceptance rates within range, or max number of iterations reached ####
    
    if (
      ( n_last_acc_rate_S>min(vn_acc_rate_range_S) && n_last_acc_rate_S<max(vn_acc_rate_range_S) )
      ## Acceptance rate S within limits
      && # AND
      (
        ( n_last_acc_rate_phi>min(vn_acc_rate_range_phi) && n_last_acc_rate_phi<max(vn_acc_rate_range_phi) )
        ## Acceptance rate phi within limits
        || # OR
        ( mcmc_prior$phi.prior=="fixed")
      )
      || # OR
      i > n_max_tuning_iterations - 1
    )
    { 
      if (i > n_max_tuning_iterations - 1)
      {
        bo_tuning_end_last_accepted <- FALSE
        cat("\n Max number of iterations succeded in PID step \n")  
      } else
      {
        bo_tuning_end_last_accepted <- TRUE
        cat("\n Last scales from tuning accepted \n")  
      }
      
      ## Scales remain the same        
      df_trace_PID[i, "new_scale_S"] <- n_S_scale
      df_trace_PID[i, "new_scale_phi"] <- n_phi_scale
      break;
    }
    
    #### PID controller ####
    ### Update S scale
    # PID controller settings
    n_K_PS <- -1.0   # Proportional
    n_K_IS <- -1.0   # Integrativive
    n_K_DS <- -0.1   # Derivative
    n_new_scale_S <- exp(n_K_PS * n_error_S + n_K_IS * n_error_S_cum + n_K_DS * n_error_S_dev)
    # We used an exp function to avoid negative values
    
    df_trace_PID[i, "new_scale_S"] <- n_new_scale_S
    n_S_scale <- n_new_scale_S
    
    ### Update phi scale
    # PID controller settings
    n_K_Pphi <- -4.0
    n_K_Iphi <- -4.0
    n_K_Dphi <- -.1    
    n_new_scale_phi <- exp(n_K_Pphi * n_error_phi + n_K_Iphi * n_error_phi_cum + n_K_Dphi * n_error_phi_dev)
    
    df_trace_PID[i, "new_scale_phi"] <- n_new_scale_phi
    n_phi_scale <- n_new_scale_phi
    
    
    
    print(df_trace_PID)
    
    
    i = i + 1
    
  }  ######## Tuning phase / PID loop START END  ########
  
  #### >>>> warm-up and eventual tuning phase ####
  
  ## The scaling parameters after the tuning phase
  (n_S_scale)
  (n_phi_scale)  
  
  
  ## Calculate back the signal; get last used phi
  n_last_thinned_step <- ncol(glmkb_tst$posterior$simulations)
  vn_S <- logit_lim(glmkb_tst$posterior$simulations[,n_last_thinned_step])
  vn_S <- vn_S - mn_X %*% vn_beta 
  n_phi <- glmkb_tst$posterior$phi$sample[n_last_thinned_step]
  
  n_thin <- round(n_final_iterations / n_final_thinned_iterations )
  
  ## Set the controls for the MCMC chain outputs
  mcmc_cntrl <- mcmc.control(S.scale   = n_S_scale, 
                             burn.in   = n_warmup_iterations,
                             thin      = n_thin, 
                             phi.scale = n_phi_scale, 
                             S.start   = vn_S,
                             n.iter    = n_final_iterations, 
                             phi.start = n_phi  )
  
  ## Set the controls for the MCMC chain outputs
  mcmc_output <-  output.glm.control(messages      = TRUE,
                                     sim.posterior = TRUE,
                                     keep.mcmc.sim = TRUE,
                                     sim.predict   = TRUE,
                                     quantile      = FALSE, #This setting saves an awful lot of calculation time!
                                     inference     = TRUE)
  tic("final MCMC")
  
  glmkb_final <- binom.krige.bayes(geod_cali,
                                   model  = mcmc_model,
                                   prior  = mcmc_prior,
                                   mcmc.input = mcmc_cntrl,
                                   output = mcmc_output,
                                   locations = str_or_geod_pred_loc
  )
  
  li_toc_final <- toc()
  li_toc_total <- toc()
  
  
  i_max <- i-1
  
  li_return <- list(
    i_max                 = i_max, 
    li_toc_final          = li_toc_final,
    li_toc_total          = li_toc_total,
    geod_cali             = geod_cali, 
    li_warming_up_results = li_warming_up_results, 
    glmkb_final           = glmkb_final,
    li_mcmc_settings      = li_mcmc_settings,
    vn_acc_rate_range_S   = vn_acc_rate_range_S,
    vn_acc_rate_range_phi = vn_acc_rate_range_phi,
    df_trace_PID          = df_trace_PID,
    bo_tuning_end_last_accepted = bo_tuning_end_last_accepted
  )
  
  
  return(li_return)
  
}

